# import the opencv library
import cv2
import math
import pygame
import time
from datetime import datetime
from deepface import DeepFace
from PIL import Image
import numpy as np
import os
import sys
import random
import time
  

def renderDetails(age, gender, race, emotion):
    global numba
    screen.fill("black")
    screen.blit(moodIco, (1650/2 - 150,920))

    #draw gender icon
    match(gender):
        case 'Man':
            screen.blit(maleIco, (120,100))
        case 'Woman':
            screen.blit(femaleIco, (120,100))
    
    #draw lvl
    text = font.render(f'lvl: {age}', True, white)
    
    screen.blit(text, (190,100))

    #draw exp
    date= datetime.utcnow() - datetime(datetime.today().year, 1, 1)
    seconds =(date.total_seconds())
    
    text = font.render(f'exp: {round(seconds)}/31536000 ({round((seconds/31536000)*100)}%)', True, white)
    
    pygame.draw.line(screen, white, (100, 100), (100, 225), 10)
    pygame.draw.line(screen, white, (100, 220), (650, 220), 10)
    pygame.draw.line(screen, lblue, (200, 205), (200 + (450 * (seconds/31536000)), 205), 10)
    screen.blit(text, (120,170))

    #draw Mood
    ymood = 0
    match(emotion):
        case 'angry':
            screen.blit(angryIco, (1200, 100 + ymood))
        case 'disgust':
            screen.blit(disgustIco, (1200, 100 + ymood))
        case 'fear':
            screen.blit(fearIco, (1200, 100 + ymood))
        case 'happy':
            screen.blit(happyIco, (1200, 100 + ymood))
        case 'sad':
            screen.blit(sadIco, (1200, 100 + ymood))
        case 'surprise':
            screen.blit(surpriseIco, (1200, 100 + ymood))
        case 'neutral':
            screen.blit(neutralIco, (1200, 100 + ymood))
    #face_analysis = DeepFace.analyze(img_path = "curr.jpg")
    #print(f"{face_analysis}")

    #draw health
    screen.blit(healthBar, (64, 883))
    remainingAge = int(age) / 80
    pygame.draw.line(screen, lgreen, (100, 900), (100+(500*(1-remainingAge)), 900), 30)
    pygame.draw.line(screen, "gray", (100+(500*(1-remainingAge)), 900), (600, 900), 30)

    #draw mana
    #biometrics
    num = random.randint(1230,1310)

    if numba == -1:
        numba = num
    else:
        num = numba

    #buiten loop verplaatsen, misschien koppelen aan de verandering van geslacht voor demo effect?
    txt1 = ""
    if num > 1290:
        txt1 = "Tip: Probeer vandaag een korte"
        advise = fontAdvise.render(txt1, True, white)
        screen.blit(advise, (1200,920))
        txt1 = "wandeling van +10 minuten."
        advise = fontAdvise.render(txt1, True, white)
        screen.blit(advise, (1200,940))
    elif num > 1260:
        txt1 = "U zit op een gezond gewicht."
        advise = fontAdvise.render(txt1, True, white)
        screen.blit(advise, (1200,920))
        txt1 = "Hou uw levenstijl vol."
        advise = fontAdvise.render(txt1, True, white)
        screen.blit(advise, (1200,940))
    else:       
        txt1 = "Tip: Eet wat vaker onverzadigde"
        advise = fontAdvise.render(txt1, True, white)
        screen.blit(advise, (1200,920))
        txt1 = "vetten zoals noten of vis."
        advise = fontAdvise.render(txt1, True, white)
        screen.blit(advise, (1200,940))
 

    pygame.draw.line(screen, white, (num,875), (num,900),10)

    text = fontAdvise.render("BMI", True, white)
    screen.blit(text, (1400, 890))
     
    pygame.draw.line(screen, white, (1200, 900), (1230, 900), 15)
    pygame.draw.line(screen, "yellow", (1230, 900), (1260, 900), 15)
    pygame.draw.line(screen, "green", (1260, 900), (1290, 900), 15)
    pygame.draw.line(screen, "orange", (1290, 900), (1320, 900), 15)
    pygame.draw.line(screen, "red", (1320, 900), (1350, 900), 15)
    pygame.draw.line(screen, "purple", (1350, 900), (1380, 900), 15)

    #1200-1380

    #skin analysis
    txt1 = "Let op: moedervlek verandering"
    advise = fontAdvise.render(txt1, True, white)
    screen.blit(advise, (1200,780))
    txt1 = "Ga langs uw huisarts voor advies."
    advise = fontAdvise.render(txt1, True, white)
    screen.blit(advise, (1200,800)) 

    text = fontAdvise.render("Huid analyze", True, white)
    screen.blit(text, (1400, 750))
     
    pygame.draw.line(screen, "cyan", (1200, 760), (1380, 760), 15)


# define a video capture object
vid = cv2.VideoCapture(0)

pygame.init()
screen = pygame.display.set_mode((1650, 1050))
clock = pygame.time.Clock()

#set colours
white = (255, 255, 255)
lblue = (68, 68, 255)
lgreen = (68, 255, 68)

#load textures
maleIco = pygame.image.load('male.png').convert()
maleIco = pygame.transform.scale(maleIco, (64, 64))
femaleIco = pygame.image.load('female.png').convert()
femaleIco = pygame.transform.scale(femaleIco, (64, 64))
sadIco = pygame.image.load('sad.png').convert()
sadIco = pygame.transform.scale(sadIco, (128, 128))
angryIco = pygame.image.load('angry.png').convert()
angryIco = pygame.transform.scale(angryIco, (128, 128))
disgustIco = pygame.image.load('disgust.png').convert()
disgustIco = pygame.transform.scale(disgustIco, (128, 128))
fearIco = pygame.image.load('fear.png').convert()
fearIco = pygame.transform.scale(fearIco, (128, 128))
happyIco = pygame.image.load('happy.png').convert()
happyIco = pygame.transform.scale(happyIco, (128, 128))
surpriseIco = pygame.image.load('surprise.png').convert()
surpriseIco = pygame.transform.scale(surpriseIco, (128, 128))
neutralIco = pygame.image.load('neutral.png').convert()
neutralIco = pygame.transform.scale(neutralIco, (128, 128))
moodIco = pygame.image.load('mood.png').convert()
moodIco = pygame.transform.scale(moodIco, (256, 128))
healthBar = pygame.image.load('barofhealth.png').convert()

#font
font = pygame.font.Font('VCR_OSD_MONO.ttf', 32)
fontAdvise = pygame.font.Font('VCR_OSD_MONO.ttf', 24)

numba = -1

isRunning = True
while(isRunning):
    pygame.display.flip()
    clock.tick(60)  # limits FPS to 60
    
      
    # Capture the video frame
    # by frame
    ret, frame = vid.read()
  
    # Display the resulting frame
    #cv2.imshow('frame', frame)
    imageRGB = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    img = Image.fromarray(imageRGB)
    
    out_file = open("curr.jpg", 'wb')
    img.save(out_file)
    out_file.flush()
    os.fsync(out_file)
    out_file.close()

    #img = img.save("curr.jpg")
    time.sleep(2)
    try:
        face_analysis = DeepFace.analyze(img_path = "curr.jpg", actions = ['age', 'gender', 'race', 'emotion'])
        #print(f"{face_analysis}")
        person = face_analysis[0]
        age = f"{person['age']}"
        gender = f"{person['dominant_gender']}"
        race = f"{person['dominant_race']}"
        emotion = f"{person['dominant_emotion']}"
        renderDetails(age, gender, race, emotion)
    except Exception as err:
        print(f"likely no face detected/{err}")
        numba = -1
    # the 'q' button is set as the
    # quitting button you may use any
    # desired button of your choice
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            isRunning = False
  
# After the loop release the cap object
vid.release()
# Destroy all the windows
cv2.destroyAllWindows()
sys.exit()