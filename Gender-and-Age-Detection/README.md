# MagicMirror Biometrics 

<h2>Objective :</h2>
<p>To build a health status detector that can approximately guess the gender, age and emotion of the person (face) in a picture or through webcam.</p>

<h2>Additional Python Libraries Required :</h2>
<ul>
  <li>OpenCV: pip install opencv-python</li>
  <li>PyGame: pip install pygame</li>
  <li>DeepFace: pip install deepface</li>
</ul>
 
 <h2>Usage :</h2>
 <ul>
  <li>Download my Repository</li>
  <li>Open your Command Prompt or Terminal and change directory to the folder where all the files are present.</li>
  <li><b>Detecting Gender and Age of face through webcam</b> Use Command :</li>
      python facedeepdetect.py
</ul>
<ul>
  <li>Press <b>Ctrl + C</b> to stop the program execution. Or close the PyGame instance</li>
</ul>